# Well-Architected Service Framework

GitLab's Well-Architected Service Framework is a lightweight set of design principals and best practices that provides guidance for running services on GitLab's platforms.

The Framework is built upon Five Pillars:

| Pillar                     | Description                                                   |
|----------------------------|---------------------------------------------------------------|
| **Reliability**            | The service's ability to recover from failures.               |
| **Security**               | Protecting services and data from threats.                    |
| **Cost optimization**      | Controlling spend to maximize the value offered by a service. |
| **Operational excellence** | Practices and Processes that protect a service's availability.|
| **Performance efficiency** | The service's ability to efficiently adapt to usage changes.  | 


<sub>[the following should be auto-generated]</sub>

## Operational Excellence

<sub>[description of Pillar]</sub>

### Service Catalog Entry

**Maturity Requirement:** All Production Services running in `Experimental` mode require this.  
**Runway Support:** ✅ <sup>Experimental</sup>

The [Service Catalog](https://gitlab.com/gitlab-com/runbooks/-/tree/master/services) is GitLab's centralized directory of Services. Each Production Service should have an entry in the directory, which records information such as service ownership, links to documentation, and alerting channels. This repository serves as an important source for many of our automated tools and processes.

Information detailing how to manually add an entry to the Service Catalog can be found in the [README](https://gitlab.com/gitlab-com/runbooks/-/blob/master/services/README.md).
